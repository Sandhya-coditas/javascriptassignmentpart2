//insertAdjacentElements
const todoList=document.querySelector(".todo-list");

//beforeend
todoList.insertAdjacentHTML("beforeend","<li>Learan Javascript</li>");

//afterbegin
todoList.insertAdjacentHTML("Afterbegin","<li>Learan Javascript</li>");

//beforebegin
todoList.insertAdjacentHTML("beforebegin","<li>Learan Javascript</li>");

//afterend
todoList.insertAdjacentHTML("Afterend","<li>Learan Javascript</li>");



//clone nodes

const ul=document.querySelector(".todo-list");
const li=document.createElement("li");
li.textContent="new todo";
const li2=li.cloneNode(true);
ul.append(li);
ul.append(li2);


//oldmethods
const ul=document.querySelector(".todo-list");

//new element
const li=document.createElement("li");
li.textContent="new todo";
//insertBefore
const referenceNode=document.querySelector(".first-todo");
ul.insertBefore(li,referenceNode);
//for replaceChild and appendChild the same syntax except the keywords
//removeChild
const referenceNode=document.querySelector(".first-todo");
ul.removeChild(referenceNode);



//static vs live list

const ul=documnet.querySelector(".todo-list");
const listItems=ul.getElementsByTagName("li");

const sixthLi=document.createElement("li");
sixthLi.textContent="item 6";

ul.append(sixthLi);
console.log(listItems);


//DIMENSIONS OF ELEMENTS(HEIGHT/WIDTH)

const sectionTodo=document.querySelector(".section-todo");
const info=sectionTodo.getBoundingClientRect();
console.log(info);
