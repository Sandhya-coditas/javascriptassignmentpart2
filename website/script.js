//query selector
const mainHeading=document.getElementById("main-heading");
const mainHeading=document.querySelector("main-heading");
const header=document.querySelector(".header");
const navItem=document.querySelectorAll(".nav-item");
console.log(navItem);


//changetext
const mainHeading=document.getElementById("main-heading");
console.log(mainHeading.textContent);
mainHeading.textContent="This is something else";
console.log(mainHeading.textContent);


//changing the element styles
const mainHeading=document.querySelector("div.headline h2");
console.log(mainHeading.style);
mainHeading.style.backgroundColor="orange";
mainHeading.style.border="2px solid black";



//get and set attributes
const link=document.querySelector("a");
console.log(link);
console.log(link.attributes("href"));
console.log(link.getAttribute("href"));

link.setAttribute("href","https.//google.com");
console.log(link.getAttribute("href"));


const inputElement=document.querySelector(".form-todo input");
console.log(inputElement.getAttribute("type"));


//get multiple items usign getElemets and querySElectorAll

//html collection
const navItems=document.getElementsByClassName("nav-items");
console.log(navItems[2]);
console.log(Array.isArray(navItems));

//nodeList
const navItems=document.querySelectorAll("nav-items");
console.log(navItems[3]);

//for loop
//Note:Cant use foreach to itereate html collection

for(let i=0;i<navItems.length;i++)
{   
    const navItems.style.backgroundColor="#fff";
    navItem.style.color="green";
    navItem.style.fontWeight="bold";

}

console.log(Array.isArray(navItems));

//nodelist
const navItems=document.querySelectorAll(".nav-item");
console.log(navItems[1]);


let navItems=document.querySelectorAll("a");
console.log(navItems);



//inner hmtl
const headline=document.querySelector(".headline");
console.log(headline.innerHTML);
headline.innerHTML="<h1>Inner html changed </h1>";
headline.innerHTML +="<button class=\btn\">Learn more</button>"
console.log(headline.innerHTML);
