//closures


//functions return other functions

function outerfunction(){
    function innerFunction(){
        console.log("Hello world")
    }
    return innerFunction;

}
const ans=outerfunction();
console.log(ans);
ans();

//Example 

function printFullName(FirstName,LastName){
    function printName(){
        console.log("FirstName,LastName")
    }
    return printName;

}
const res=printFullName("SAndhya","RAni");
console.log(res);
res();

//Example

function hello(x){
    const a="varA";
    const b="varB";
    return function(){
        console.log(a,b,x);
    }

}
const ans_=hello("arg");
ans_();



//Example
function myFunction(power){
    return function(number){
        return number ** power
    }
    }

const cube=myFunction(3);
const res2=cube(2);
console.log(res2);

const square=myFunction(3);
const res3=square(2);
console.log(res3);



//Example

function fun(){
    let counter=0;
    return function(){
        if(counter<1){ //when condition is true
            console.log("Hi you called me...")
            counter++;
        }
        else{
            console.log("I already called u onvce before")
        }
    }

    const myfun=fun();
    myfun();
    myfun();
    const myfun2=fun();
    myfun2();
    myfun2();
}