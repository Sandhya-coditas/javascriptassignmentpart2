//hoisting
console.log(this);
console.log(window);
console.log(firstName);
var firstName="Sandhya";
console.log(firstName);

console.log(myFunction);
 
//Global vs Local Scope
function myFunction(){
    console.log("This is my function");
}

var firstName="Sandhya";
var lastName="Shanna";
var fullName="firstName"+" "+"lastName";
console.log(fullName);


var myfunction=function(){
    console.log("This is my function");
}
myfunction();

52


